--1)

--a)
create database teste;
-- A rotina create database é ilustrativa 

--b)

create table cliente (codigo int, nome varchar(30));

--c)
alter table cliente add column endereco varchar(60) not null;

--d)
alter table cliente alter column endereco drop not null;

--e)
alter table cliente add constraint cliente_codigo_pk primary key (codigo);

--f)
alter table cliente add column ativado char;

--g)
alter table cliente add constraint cliente_ativado_check check (ativado in ('A','I'));

--h)
insert into cliente values(1,'Aline','Rua-1','A'),(2,'Lucio','Rua-2','A'),(3,'Carlos','Rua-3','A');

--i)
update cliente set ativado='I' where nome='Aline'

--2)
--a)
create table produto(codigo int primary key, descricao varchar(60));

--b)
alter table produto add column cod_clie int;

--c)
alter table produto add constraint produto_codclie_fk foreign key (cod_clie) references cliente;

--d)
--a)
alter index produto_pkey rename to produto_codigo_pkey; -- Versões mais novas do postgres

--b) 
alter table produto drop constraint produto_pkey;
alter table produto add constraint produto_codigo_pkey primary key(codigo);

--e)
alter table produto add column preco numeric(7,2);

--f)
alter table produto add constraint preco_check check (preco >= 0.00) -- Não pode ser negativo

--g) 
insert into produto values(1,'Arroz',1,9.00),(2,'Feijao',1,5.00);

--h)
insert into produto values(3,'Arroz',2,11.90),(4,'Carne',2,27.80),(5,'Batata',2,7.00);

--i)
insert into produto values(6,'Batata',3,8.70),(7,'Tomate',3,4.50);

--3)
--3.1a)
select c.nome,p.descricao,p.preco
from cliente c inner join produto p on (c.codigo=p.cod_clie and p.preco > 5.00)

--3.1b)
select c.nome,p.descricao,p.preco
from cliente c inner join produto p on (c.codigo=p.cod_clie)
where p.preco > 5.00

--3.2a)
select c.nome, count(p.descricao) quantidade
from cliente c inner join produto p on (c.codigo=p.cod_clie)
group by c.nome
order by quantidade desc

--3.3a)
select c.nome, sum(p.preco) total
from cliente c inner join produto p on (c.codigo=p.cod_clie)
group by c.nome
order by total desc

