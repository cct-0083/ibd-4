-- Exercicio 12 Respostas consultas

-- a)
-- b)
-- c)

select c.nome, sum(pa.valor_pago)
from colaborador c join participante pa on (c.codigo=pa.cod_col)
join projeto p on (pa.cod_proj=p.codigo)
where p.descricao in ('WEB')
group by c.nome
order by 1 desc

--d)
--d.a)
-- sem subconsulta
select c.nome
from cliente c join colaborador co on (c.cpf=co.cpf)

--d.b)
select nome,cpf
from cliente
intersect
select nome,cpf
from colaborador

--d.c)
select nome
from cliente
where cpf in (select cpf from colaborador)

--e)
select co.nome, sum(pa.valor_pago)
from colaborador co join participante pa on (co.codigo = pa.cod_col)
where co.depto = 'SAC'
group by co.nome
having (sum(pa.valor_pago) > all (select sum(pa.valor_pago)
 from participante pa join colaborador co on (co.codigo = pa.cod_col)
where co.depto='RH'
group by co.nome))

--f)
select cidade
from cliente
union
select cidade
from colaborador

--g)
--g.a) 
select c.nome
from colaborador c
where c.depto in ('SAC','TI') and c.cidade = 'Brasilia' 

--g.b)
select c.nome
from colaborador c
where c.depto ='SAC' or c.depto='TI' and c.cidade = 'Brasilia' 

--h)
select co.nome, sum(pa.valor_pago)
from participante pa join colaborador co on (pa.cod_col = co.codigo)
where co.depto='SAC'
group by co.nome
having (sum(pa.valor_pago ) < ALL (select sum(pa.valor_pago) total
from participante pa join colaborador co on (pa.cod_col = co.codigo)
where co.depto='TI'
group by co.nome))

--i)
select co.nome, sum(pa.valor_pago)
from colaborador co join participante pa on (co.codigo=pa.cod_col)
group by co.nome
order by 2 desc, 1 asc

--j)

select co.nome, sum(pa.valor_pago)
from colaborador co join participante pa on (co.codigo=pa.cod_col)
group by co.nome
order by 2 desc, 1 asc

--k)
select co.depto, sum(pa.valor_pago)
from colaborador co join participante pa on (co.codigo=pa.cod_col)
group by co.depto

