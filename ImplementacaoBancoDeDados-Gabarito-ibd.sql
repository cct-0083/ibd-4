1) d)
2) c)
3) b)
4) 
   a) Errado
   b) Certo
   
    creata table teste (
    cpf integer,
    cep char(8),
    ordem int,
    nome varchar(30),
    constraint "chave_primaria" primary key (cpf,cep,ordem);
    )
    
   c) Certo

5) c) 
6) a(V)
   b(V)
   c(V)
   d(V)
   e(F) alter table compra rename valor to pagamento;
   f(F)   

7) a)

8) 
--a)

create table cliente (
	codigo int
	
)

--b) 

alter table cliente add column ordem int;

--c) 

alter table cliente add primary key(codigo, ordem);

--ou

alter table cliente add constraint chave_primaria primary key(codigo,ordem);

--d)
alter table cliente add column nome varchar(60);

--e)
alter table cliente add column sobrenome varchar(60);

--f)
alter table cliente drop column sobrenome;

--g)
alter table cliente alter column nome set not null;

--h)
alter table cliente alter column nome drop not null;

--i)
insert into cliente(codigo,ordem) values(1,1); 

--j)
update cliente
set nome='Ana'
where codigo=1 and ordem=1

--k)
alter table cliente drop constraint chave_primaria;
alter table cliente add constraint cliente_pk primary key (codigo,ordem);

--ou
alter index chave_primaria rename to cliente_pk;

9) 


   