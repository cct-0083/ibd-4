create table colaborador (
 codigo int,
 cpf char(1),
 nome varchar(30),
 cidade varchar(60),
 depto varchar(3),	
 constraint colaborador_pk primary key(codigo)	
);

create table cliente (
 codigo int,
 cpf char(1),
 nome varchar(30),
 cidade varchar(60),
 constraint cliente_pk primary key(codigo)	
);

create table projeto (
	codigo int,
	descricao varchar(60),
	constraint projeto_pk primary key(codigo)
);

create table participante (
	cod_col int,
	cod_proj int,
	valor_pago decimal(10,2),
	constraint participante_pk primary key(cod_col,cod_proj),
	constraint projeto_fk foreign key(cod_proj) references projeto,
	constraint colaborador_fk foreign key(cod_col) references colaborador
);

insert into colaborador values (1,'1','Ana','Rio Verde','RH'),(2,'2','Luana','Goiania','TI'),
(3,'3','Gustavo','Brasilia','SAC'),(4,'4','Bruna','Brasilia','TI');

insert into cliente values (1,'1','Ana','Rio Verde'),(2,'7','George','Belo Horizonte'),
(3,'3','Gustavo','Brasilia'),(4,'8','Lorena','Brasilia');

insert into projeto values (1,'Website'),(2,'Engenharia'),(3,'Banco de Dados');

insert into participante values(1,1,4500.00),(2,1,5570.00),(4,1,13785.00),(2,2,7800.00),
(3,1,4000.00),(3,2,3750.00),(3,3,7200.00),(4,2,2500.00);